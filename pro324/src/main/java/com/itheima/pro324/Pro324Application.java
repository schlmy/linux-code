package com.itheima.pro324;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pro324Application {

    public static void main(String[] args) {
        SpringApplication.run(Pro324Application.class, args);
    }

}
